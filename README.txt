This is a simple module meant to help in situations where you the devel module's dpm function
does not work, such as when debugging cron, ajax calls, form submissions.

Instead of adding dpm($data):
If you are working interactively and a new page will be displayed immediately after your
action where you want your data to be displayed (like using dpm()), use dqdpm(). This
mimics the behavior of dpm(), except that it writes your data to a cache table,
and then displays it on whatever page is rendered next.

If you need to queue your debug data for retrieval/display at some arbitrary point
later on, insert a call to debug_queue_add(), which will save your data to the database, where
you can subsequently inspect it, either by inserting a call to debug_queue_dpm() somewhere else,
or by going to /debug_queue/<your debug queue identifier>.

The arguments to the debug_queue_add() function are:
  $dqid = a string that identifies your debug queue
  $key = a string that identifies the data you are saving
  $data = the data that you are saving

Example:
$yinyang = array('yin', 'yang');
debug_queue_add('my_queue', 'yinyang', $yinyang);

You can then go to /debug_queue/my_queue, where you will see a krumo display of the contents of
my_queue (if you do not have the devel module installed, you will see a warning to that effect).

There are a couple functions for viewing the contents of a debug queue, debug_queue_dpm() and
debug_queue_krumo(). These also require the devel module to function.

You can call debug_queue_read() if you want to get the contents of your debug_queue and do something
it.

Consult the code in debug_queue.module for more details.

REQUIREMENTS: There are no requirements to install this module, however it is not very useful without
the devel module.

INSTALLATION: Place the debug_queue folder into your modules folder. Enable the module. This module
should be not be enabled on production sites.
